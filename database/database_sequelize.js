const Sequelize = require('sequelize');
const sequelize = new Sequelize('cctest_db', 'root', 'secret', {
  host: 'localhost',
  dialect: 'mysql',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

sequelize.authenticate()
.then(() => {
  console.log('Connection sequelize success!');
})
.catch(err => {
  console.log('Connectino sequelize error!');
});

module.exports = {
  sequelize
};