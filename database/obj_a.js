const database = require('./database.js');
const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const SchemaObjA = new Schema({
	id: ObjectId,
	name: String,
	number: Number
});

const ModelObjA = database.connection.model('ModelObjA', SchemaObjA);

module.exports = {
	model: ModelObjA,
	new_a: (data) => {
		return new ModelObjA({ name: data['name'], number: data['number'] });
	}
};