const mongoose = require('mongoose');
const URI = 'mongodb://localhost:27017/ccDB';

mongoose.connect(URI, { useNewUrlParser: true });
let conn = mongoose.connection;
conn.on('error', console.error.bind(console, 'connection error:'));	
conn.once('open', () => {
	console.log('connected!');
});

module.exports = {
	connection: conn
};