const Sequelize = require('sequelize');
const sequelize = require('./../database/database_sequelize.js').sequelize;

const User = sequelize.define('user', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
	name: {
    type: Sequelize.STRING
  }
});

// force: true will drop the table if it already exists
User.sync({force: false}).then(() => {
  // Table created
});

module.exports = {
	user: User
};