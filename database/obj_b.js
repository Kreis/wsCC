const database = require('./database.js');
const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const SchemaObjB = new Schema({
	id: ObjectId,
	name: String,
	number: Number
});

const ModelObjB = database.connection.model('ModelObjB', SchemaObjB);

module.exports = {
	model: ModelObjB,
	new_b: (data) => {
		return new ModelObjB({ name: data['name'], number: data['number'] });
	}
};