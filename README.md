# wsCC

Ejemplo WebService para uso de tecnologías; ExpressJs, Mongoose, sequelize, JWT, mochajs, etc.

## Empezando

### Prerequisitos
#### MongoDB
Requiere installar MongoDB v4.0.0 o superior
Generar la carpeta /data/db asociada según corresponda

#### MySql
Requiere installar MySql Server 5.7 o superior
https://dev.mysql.com/downloads/mysql/
### Configuración
```
	host: "localhost",
	user: "root",
	password: "secret",
	database_name: "cctest_db"
```

### Instalando
Instrucciones por consola
```
> git clone https://gitlab.com/Kreis/wsCC.git

> cd wsCC

> npm install

> npm start

```

### Rutas
#### Login
Obtener token JWT para futuras autorizaciones
```
localhost:3000/login
```

Para autorización en endpoints
key: Authorization
value: bearer <Token>

#### Tiempo actual
Obtener en milisegundos la hora local del servidor
```
localhost:3000/time
```

#### Guardar elemento
Guardar elemento en base de datos mongo, number que causa error (para pruebas) 777
```
localhost:3000/add/:name/:number
```

#### Obtener elemento
Obtener elemento de base de datos mongo, con el Id obtenido de 'localhost:3000/add/:name/:number'
```
localhost:3000/get_obj/:id
```

#### Guardar nombre
Guarda elemento en base de datos MySQL
```
localhost:3000/save_name/:name
```

### Pruebas unitarias
```
npm test
```

