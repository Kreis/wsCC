const user = require('./../database/user_sequelize.js').user;

module.exports = {
	save_name: (name, success, failure) => {
		user.create({
			name
		}).then(() => {
			success();
		}).catch(() => {
			failure();
		});
	}
}