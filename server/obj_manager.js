const obj_a = require('./../database/obj_a.js');
const obj_b = require('./../database/obj_b.js');

module.exports = {
	save_obj_a: (data, success, failure) => {
		const a = obj_a.new_a(data);
		a.save((err) => {
			if (err)
				failure();
			
			// forbidden number to force an error
			if (data['number'] == 777)
				data['number'] = 'CRASH_PLEASE';

			const b = obj_b.new_b(data);
			b.save((err) => {
				if (err) {
					obj_a.model.deleteOne({ '_id': a._id }, (err) => {
						if (err)
							console.log('Error: A collection fail to delete element ' + a._id);
						else
							console.log('A collection delete element ' + a._id);
						failure();
					});
				} else {
					success({'id': a._id});
				}
			});
		});
	},

	get_obj_a: (_id, success, failure) => {
		obj_a.model.findOne({ _id: _id }, (err, finded) => {
			if (finded)
				success({
					'name': finded.name,
					'number': finded.number
				});
			else
				failure();
		});
	}
}