const assert = require('assert');
const obj_manager = require('./../server/obj_manager.js');

describe('Database save data', () => {
  describe('#save_obj_a(data)', () => {
    it('should save data and get an id', () => {
    	
    	const data = {
    		'name': 'abc123',
    		'number': 202020
    	};
    	const success = (result) => {
    		assert(result['id']);
    	};
    	const failure = () => {
    		assert(false);
    	}
    	obj_manager.save_obj_a(data, success, failure);
    });
  });
});