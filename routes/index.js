const time = require('../server/time.js');
const obj_manager = require('../server/obj_manager.js');
const name_manager = require('../server/name_manager.js');
const jwt = require('jsonwebtoken');
const express = require('express');
const router = express.Router();
const crypto = require('crypto');
const SECRET_KEY = 'WKzMBo4Po0kf9B45ZBt3eIUCEkAvAL+qKqcD/xsl';

// MIDDLEWARE
const verify_token = (req, res, next) => {
	const bearer_header = req.headers['authorization'];
	if (typeof bearer_header === 'undefined') {
		res.sendStatus(403);
		return;
	}

	const bearer_token = bearer_header.split(' ')[ 1 ];

	jwt.verify(bearer_token, SECRET_KEY, (err, auth_data) => {
		if (err) {
			res.sendStatus(403);
			return;
		}
		const password = auth_data['data']['password'];
		const hash_password = crypto.createHash('sha256').update(password).digest('base64');
		if (auth_data['data']['id'] != 1 || hash_password != 'XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=') {
			res.sendStatus(403);
			return;
		}

		next();
	});
}

// ROUTES
router.get('/login', (req, res, next) => {
	const data = {
		'id': req.headers['id'],
		'password': req.headers['password']
	};

	jwt.sign({data}, SECRET_KEY, { expiresIn: '120s' }, (err, token) => {
		res.json({ token });
	});
});

router.get('/time', verify_token, (req, res, next) => {
	res.json( { 'current_time': time.get_time() });
});

router.post('/add/:name/:number', verify_token, (req, res, next) => {
	new Promise((success, failure) => {
		const data = { 'name': req.params.name, 'number': req.params.number };
		obj_manager.save_obj_a(data, success, failure);
	}).then((result) => {
		res.json({
			'result': 'success',
			'id': result['id']
		});
	}).catch(() => {
		res.json({
			'result': 'failure',
			'description': 'invalid data'
		});
	});
});

router.get('/get_obj/:id', verify_token, (req, res, next) => {
	new Promise((success, failure) => {
		obj_manager.get_obj_a(req.params.id, success, failure);
	}).then((result) => {
		res.json({
			'result': 'success',
			'name': result['name'],
			'number': result['number']
		});
	}).catch(() => {
		res.json({
			'result': 'failure',
			'description': 'object not found'
		});
	});
	
});

router.post('/save_name/:name', (req, res, next) => {
	new Promise((success, failure) => {
		name_manager.save_name(req.params.name, success, failure);
	}).then(() => {
		res.json({
		'result': 'success'
		});
	}).catch(() => {
		res.json({
		'result': 'failure'
		});
	});
	
});

module.exports = router;
